import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotFoundComponent, PaginationComponent } from '@shared/components';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [NotFoundComponent, PaginationComponent],
  exports: [NotFoundComponent, PaginationComponent]
})
export class SharedModule {}
