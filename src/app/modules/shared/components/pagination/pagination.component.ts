import {
  Component,
  OnInit,
  Input,
} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  private _currentPage: number;

  @Input() set currentPage(value: number) {
    this._currentPage = value;
    this._setPagination();
  }

  get currentPage(): number {
    return this._currentPage;
  }

  public pages: Array<number>;

  constructor() {}

  ngOnInit() {}

  private _setPagination() {
    const total = this.currentPage + 4;
    const pagesArr = [...Array(total).keys()].filter(Number);
    this.pages = pagesArr.slice(Math.max(pagesArr.length - 5, 0));
  }
}
