import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MovieRoutingModule } from './movie-routing.module';
import {
  MovieComponent,
  MovieBarComponent,
  MovieDisplayComponent,
  MovieCastComponent,
  MovieSidebarComponent
} from '@movie/components';

@NgModule({
  declarations: [
    MovieComponent,
    MovieBarComponent,
    MovieDisplayComponent,
    MovieCastComponent,
    MovieSidebarComponent
  ],
  imports: [CommonModule, MovieRoutingModule]
})
export class MovieModule {}
