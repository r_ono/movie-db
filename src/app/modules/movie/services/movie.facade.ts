import { Injectable } from '@angular/core';
import { zip } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiService } from '@core/services';
import { IRequest, backdropSizes, posterSizes, profileSizes } from '@core/interfaces';
import { IMovie, ICredits, IKeyWordsList } from '@movie/interfaces';
import { HomeFacade } from '@home/services';

@Injectable({
  providedIn: 'root'
})
export class MovieFacade {

  constructor(
    private _api: ApiService,
    private _home: HomeFacade
  ) { }

  getMovie(id: string) {



    return zip(
      this._getMovieInfo(id),
      this._getMovieCredits(id),
      this._getMovieKeywords(id),
      this._home.baseImageUrl$
    ).pipe(
      map(([data, credits, keywords, url]) => {
        data.backdrop_path = `${url}${backdropSizes.w1280}${data.backdrop_path}`;
        data.poster_path = `${url}${posterSizes.w342}${data.poster_path}`;

        credits.cast.forEach( cast => {
          cast.profile_path = cast.profile_path && `${url}${profileSizes.w185}${cast.profile_path}`;
        });

        credits.crew.forEach( person => {
          person.profile_path = person.profile_path && `${url}${profileSizes.w185}${person.profile_path}`;
        });

        data.credits = credits;
        data.keywords = keywords.keywords;

        return data;
      })
    );
  }

  private _getMovieInfo(id: string) {
    const movieRequest: IRequest = {
      endPoint: `movie/${ id }`,
    };

    return this._api.get<IMovie>(movieRequest);
  }

  private _getMovieCredits(id: string) {
    const creditRequest: IRequest = {
      endPoint: `movie/${ id }/credits`,
    };

    return this._api.get<ICredits>(creditRequest);
  }

  private _getMovieKeywords(id: string) {
    const creditRequest: IRequest = {
      endPoint: `movie/${ id }/keywords`,
    };

    return this._api.get<IKeyWordsList>(creditRequest);
  }
}
