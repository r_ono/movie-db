import { Component, OnInit, Input } from '@angular/core';

import { IMovie } from '@movie/interfaces';

@Component({
  selector: 'app-movie-display',
  templateUrl: './movie-display.component.html',
  styleUrls: ['./movie-display.component.scss']
})
export class MovieDisplayComponent implements OnInit {

  @Input() movie: IMovie;

  constructor() { }

  ngOnInit() {
  }

}
