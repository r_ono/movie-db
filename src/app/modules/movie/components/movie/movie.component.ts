import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { MovieFacade } from '@movie/services';
import { IMovie } from '@movie/interfaces';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  movie$: Observable<IMovie>;

  constructor(
    private _route: ActivatedRoute,
    private _movieFacade: MovieFacade
  ) { }

  ngOnInit() {

    const id = this._route.snapshot.paramMap.get('id');
    this.movie$ = this._movieFacade.getMovie(id);
  }

}
