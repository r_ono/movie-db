export * from './movie/movie.component';
export * from './movie-bar/movie-bar.component';
export * from './movie-cast/movie-cast.component';
export * from './movie-display/movie-display.component';
export * from './movie-sidebar/movie-sidebar.component';
