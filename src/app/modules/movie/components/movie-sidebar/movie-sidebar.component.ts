import { Component, OnInit, Input } from '@angular/core';
import { IMovie } from '@movie/interfaces';

@Component({
  selector: 'app-movie-sidebar',
  templateUrl: './movie-sidebar.component.html',
  styleUrls: ['./movie-sidebar.component.scss']
})
export class MovieSidebarComponent implements OnInit {

  @Input() movie: IMovie;

  constructor() { }

  ngOnInit() {
  }

}
