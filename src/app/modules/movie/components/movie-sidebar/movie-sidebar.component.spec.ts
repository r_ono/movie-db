import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieSidebarComponent } from './movie-sidebar.component';

describe('MovieSidebarComponent', () => {
  let component: MovieSidebarComponent;
  let fixture: ComponentFixture<MovieSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
