import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieBarComponent } from './movie-bar.component';

describe('MovieBarComponent', () => {
  let component: MovieBarComponent;
  let fixture: ComponentFixture<MovieBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
