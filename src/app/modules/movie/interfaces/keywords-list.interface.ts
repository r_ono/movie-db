import { IKeyWords } from './keywords.interface';

export interface IKeyWordsList {
  id: number;
  keywords: Array<IKeyWords>;
}
