import {
  IGenres,
  ICompanies,
  IProductionCountries,
  ISpokenLanguage,
  ICredits,
  IKeyWords
} from '@movie/interfaces';

export interface IMovie {
  adult: boolean;
  backdrop_path: string;
  belongs_to_collection: null;
  budget: number;
  genres: Array<IGenres>;
  homepage: string;
  id: number;
  imdb_id: string;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  production_companies: Array<ICompanies>;
  production_countries: Array<IProductionCountries>;
  release_date: string;
  revenue: number;
  runtime: number;
  spoken_languages: Array<ISpokenLanguage>;
  status: string;
  tagline: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
  credits: ICredits;
  keywords: Array<IKeyWords>;
}
