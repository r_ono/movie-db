import { ICast, ICrew } from '@movie/interfaces';

export interface ICredits {
  id: number;
  cast: Array<ICast>;
  crew: Array<ICrew>;
}
