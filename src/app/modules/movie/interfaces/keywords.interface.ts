export interface IKeyWords {
  id: number;
  name: string;
}
