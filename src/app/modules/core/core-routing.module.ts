import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from '@shared/shared.module';
import { NotFoundComponent } from '@shared/components';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('../home/home.module').then(m => m.HomeModule)
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ],
  exports: [RouterModule]
})
export class CoreRoutingModule {}
