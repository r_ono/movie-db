import { createSelector } from '@ngrx/store';
import { imageState, IImageState } from '@core/store/state';

export const selectImageBaseUrl = createSelector(
  imageState,
  (state: IImageState) => state.secure_base_url
);
