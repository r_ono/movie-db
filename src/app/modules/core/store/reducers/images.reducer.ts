import { loadConfig } from '@core/store/actions';
import { createReducer, Action, on } from '@ngrx/store';
import { initialImageState, IImageState } from '@core/store/state';

const reducer = createReducer(
  initialImageState,
  on(loadConfig, (state, payload) => ({
    ...state,
    ...payload.images,
  }))
);

export function imageReducer(state: IImageState | undefined, action: Action) {
  return reducer(state, action);
}
