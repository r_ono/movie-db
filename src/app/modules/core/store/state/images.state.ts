import { AppState } from '@core/store';
import {
  backdropSizes,
  logoSizes,
  posterSizes,
  profileSizes,
  stillSizes
} from '@core/interfaces';

export interface IImageState {
  base_url: string;
  secure_base_url: string;
  backdrop_sizes: Array<backdropSizes>;
  logo_sizes: Array<logoSizes>;
  poster_sizes: Array<posterSizes>;
  profile_sizes: Array<profileSizes>;
  still_sizes: Array<stillSizes>;
  change_keys: Array<string>;
  loaded: boolean;
}

export const initialImageState: IImageState = {
  base_url: null,
  secure_base_url: null,
  backdrop_sizes: [],
  logo_sizes: [],
  poster_sizes: [],
  profile_sizes: [],
  still_sizes: [],
  change_keys: null,
  loaded: false
};

export const imageState = (state: AppState) => state.images;
