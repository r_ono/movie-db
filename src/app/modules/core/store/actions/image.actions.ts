import { createAction, props } from '@ngrx/store';
import { IConfigResult } from '@core/interfaces';

export const loadConfig = createAction(
  '[Configuration] Images',
  props<IConfigResult>()
);
