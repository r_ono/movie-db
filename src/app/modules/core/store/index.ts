import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '@app/environments/environment';
import { IImageState } from '@core/store/state';
import { imageReducer } from '@core/store/reducers';

export interface AppState {
  images: IImageState;
}

export const reducers: ActionReducerMap<AppState> = {
  images: imageReducer
};


export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
