import { IImageState } from '@core/store/state';

export interface IConfigResult {
  images: IImageState;
}
