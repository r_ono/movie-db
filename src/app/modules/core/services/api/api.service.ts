import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@app/environments/environment';
import { IRequest } from '@core/interfaces/request.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _apiUrl = environment.apiUrl;
  private _apiKey = environment.apiKey;

  constructor(private _http: HttpClient) {}

  public get<T>(request: IRequest): Observable<T> {
    const options = {
      params: {
        ...request.queryString,
        api_key: this._apiKey
      }
    };

    return this._http.get<T>(`${this._apiUrl}/${ request.endPoint }`, options);
  }

}
