import { Component, OnInit } from '@angular/core';
import { HomeFacade } from '@home/services';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public isLoaded = false;

  constructor(private _homeFacade: HomeFacade) {}

  ngOnInit() {
    this._homeFacade
      .loadConfiguration()
      .pipe(take(1))
      .subscribe(isLoaded => (this.isLoaded = !!isLoaded));
  }
}
