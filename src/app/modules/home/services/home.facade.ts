import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

import { AppState } from '@core/store';
import { ApiService } from '@core/services';
import { IRequest, IConfigResult } from '@core/interfaces';
import { loadConfig } from '@core/store/actions';
import { selectImageBaseUrl } from '@core/store/selectors';

@Injectable({
  providedIn: 'root'
})
export class HomeFacade {

  baseImageUrl$ = this._store.select(selectImageBaseUrl);

  constructor(
    private _store: Store<AppState>,
    private _api: ApiService
  ) { }

  loadConfiguration() {
    const request: IRequest = {
      endPoint: 'configuration',
    };

    return this._api.get<IConfigResult>(request).pipe(
      switchMap( config => {
        this._store.dispatch(loadConfig(config));
        return of(config);
      })
    );
  }
}
