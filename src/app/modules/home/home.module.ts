import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import {
  HomeComponent,
  NavBarComponent,
  StartComponent,
  FooterComponent
} from '@home/components';

@NgModule({
  declarations: [HomeComponent, NavBarComponent, StartComponent, FooterComponent],
  imports: [CommonModule, HomeRoutingModule]
})
export class HomeModule {}
