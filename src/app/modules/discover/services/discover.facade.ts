import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { zip } from 'rxjs';

import { ApiService } from '@core/services';
import { IRequest, posterSizes } from '@core/interfaces';
import { IDiscoverResult } from '@discover/interfaces';
import { HomeFacade } from '@home/services';

@Injectable({
  providedIn: 'root'
})
export class DiscoverFacade {
  constructor(
    private _api: ApiService, private _home: HomeFacade) {}

  listMovies(page: string) {
    const request: IRequest = {
      endPoint: 'discover/movie',
      queryString: {
        page
      }
    };

    return zip(
      this._api.get<IDiscoverResult>(request),
      this._home.baseImageUrl$
    ).pipe(
      map(([data, url]) => {
        data.results.forEach(movie => {
          movie.poster_path = `${url}${posterSizes.w154}${movie.poster_path}`;
        });

        return data;
      })
    );
  }
}
