import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DiscoverRoutingModule } from './discover-routing.module';
import { DiscoverComponent, MovieCardComponent } from '@discover/components';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [DiscoverComponent, MovieCardComponent],
  imports: [CommonModule, DiscoverRoutingModule, SharedModule]
})
export class DiscoverModule {}
