import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiscoverComponent } from '@discover/components';

const routes: Routes = [
  {
    path: '',
    component: DiscoverComponent
  },
  {
    path: ':page',
    component: DiscoverComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiscoverRoutingModule { }
