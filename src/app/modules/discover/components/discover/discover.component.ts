import { IDiscoverResult } from '@discover/interfaces';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { DiscoverFacade } from '@discover/services';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss']
})
export class DiscoverComponent implements OnInit, OnDestroy {
  public list$: Observable<IDiscoverResult>;

  private _unsub$ = new Subject();
  public page: number;

  constructor(
    private _route: ActivatedRoute,
    private _discoverFacade: DiscoverFacade
  ) {}

  ngOnInit() {
    this._route.paramMap.pipe(takeUntil(this._unsub$)).subscribe(params => {
      const page = params.get('page') || '1';
      this.page = Number(page);
      this.list$ = this._discoverFacade.listMovies(page);
    });
  }

  ngOnDestroy() {
    this._unsub$.next();
    this._unsub$.complete();
  }
}
