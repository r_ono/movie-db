import { Component, OnInit, Input } from '@angular/core';

import { IMovieCard } from '@discover/interfaces';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent implements OnInit {
  @Input() movie: IMovieCard;

  constructor() {}

  ngOnInit() {}

  getPoster(file: string) {
    // return `https://image.tmdb.org/t/p/${posterSizes.w154}/${file}`;
  }
}
