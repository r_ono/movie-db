import { IMovieCard } from './movie-card.interface';
export interface IDiscoverResult {
  page: number;
  results: Array<IMovieCard>;
  total_pages: number;
  total_results: number;
}
